# Solution à la découpeuse laser

Nous avons fait valider [ce modèle sur Thingiverse conçu par DrBioPhysics](https://www.thingiverse.com/thing:4155735) par l'hôpital CHU Saint-Pierre à Bruxelles.

![](../images/P2-summary.jpeg)

## Matériel

* Matériel
  * plaque de plexiglas en 3 mm
  * feuille A4 transparente
* Outils :
  * découpeuse laser
  * une foreuse + mêche 3.2 mm
* Temps de production : 1 minute par masque

## Processus de fabrication

### Etape 1 - Découpe de la structure

Téléchargez les fichiers SVG optimisés pour une plaque 100x60cm ci-dessous et découpez-les à la découpeuse laser.

* [SVG 30 pièces (100x60cm)](files/Mask_30pcs_overcut.svg)
* [SVG 33 pièces (100x60cm)](files/Mask_33pcs.svg)

![](../PFC-Cap-LaserCut/images/P2-30Masks.png)

### Etape 2 - Trouer la feuille A4 transparente

D'après [ce modèle sur Thingiverse conçu par LadyLibertyHK](http://thingiverse.com/thing:4159366).

A l'aide d'une foreuse et d'une mèche 3.2 mm, trouer une pile de 10 feuilles A4 transparentes selon [le patron suivant](docs/PFC-Cap-LaserCut/files/PFC-Cap-garabit-transparent.DXF).

![](../PFC-Cap-LaserCut/images/P2-transparent-trous.jpeg)
![](../PFC-Cap-LaserCut/images/cap-gabarit.PNG)

### Etape 3 - Assembler

l'assemblage se fait à l'hôpital.

## Auteurs et remerciements

Tutoriel réalisé par Denis Terwagne, (Frugal LAB & Fablab ULB, Université Libre de Bruxelles, Belgique). Merci à toute l'équipe pour votre contribution et vos photos.
