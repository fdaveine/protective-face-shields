# 3ter - Solution en bandeau flexible au cutter


Cette solution a été inspirée d’un design de Dr Carmen Ionescu, du service d'anesthésie de l'hopital d'Ixelles, et partagée sous une [licence Creative Commons BY-NC-SA 3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/). Elle est très rapide à produire et ne nécessite aucune machine technologique de découpe.



**Cette version est une alternative du [bandeau flexible à la découpe laser](../PFC-Headband-Flexible-LaserCut/tuto-fr.md).**

![](files/headband-flexible-cutter-general.jpg)

## Matériel
* Matériel :
  * Une feuille de PVC A4 transparente.
  * Une feuille de Priplak® (polypropylene), d’épaisseur 1mm, de 70 cm de longueur minimum.
* Outils :
  * Cutter ou une paire de ciseaux
* Temps de production : 5min


## Processus de fabrication

### Etape 1 – Découpe de la structure (« bandelette » ci-dessous)

Découpez une longue bande de 20mm de largeur (peut être adaptée à 30mm) :


![](files/headband-flexible-dimensions.png)
![](files/headband-flexible-cutter-cut1.jpg)


Ensuite, découpez l’extrémité en forme de T pour servir d’accroche de la bande :


![](files/headband-flexible-cutter-cut2.png)

Réalisez un trou pour l’encoche. Prenez la mesure de votre tour de tête pour y placer le trou à l’emplacement qui vous correspond. Pour réaliser ce trou, faites simplement une croix au cutter.


![](files/headband-flexible-cutter-cut3.png)

Rabattez les deux extrémités et coupez-les au cutter pour éviter qu’ils ne dépassent.

![](files/headband-flexible-cutter-cut4.png)


Finalement, découper le « T » initial pour permettre une insertion plus aisée dans les entailles de la feuille plastique :

![](files/headband-flexible-cutter-cut5.jpg)


## Etape 2 - Découpez la feuille A4 transparente
Découpez dans la feuille transparente, 4 entailles de 20mm de long situées comme sur le plan ci-dessous (en mm). Il a été constaté qu’une entaille ajustée (de dimensions égales à la largeur des bandes) est nécessaire pour un meilleur maintien de la feuille transparente.

![](files/headband-flexible-sheet-dimensions.png)


Pour simplifier les découpes et l’assemblage, une version avec 2 entailles est également possible pour un maintien suffisant du masque :


![](files/headband-flexible-sheet-dimensions2.png)

## Etape 3 – Assembler
L’assemblage se fait à l’hôpital, mais est décrit pour information :
1. Insérer la bandelette dans les entailles sur toute la longueur de la feuille

![](files/headband-flexible-assembly1.png)

2. L’accroche à l’arrière de la tête se fait simplement comme suit :

![](files/headband-flexible-assembly2.png)

3. L’ajustement de la visière se fait directement sur la tête de l’utilisateur. La visière tient à distance du front par frottement.

![](files/headband-flexible-assembly3.png)


## Auteurs et remerciements

Tutoriel réalisé par Loïc Blanc (BEAMS & Fablab ULB). Merci à toute l'équipe pour votre contribution et vos photos.
