# Protective mask 3D printed.

This is the 3D printed version of the protective mask fabrication effort at FabLab ULB (Brussels - Belgium).

This design and documentation were written by Nicolas H.-P. De Coster &lt;ndcoster@meteo.be&gt; and delivered under [MIT license](./LICENSE.md).

## Important note

Make sure you have first read the general documentation (1 folder ahead) about hospital validation and history.

Using the .scad file instead of provided pre-compiled `.stl` allows you fine-tune the best parameters for your printer and get (much) better results and printing speed.

This very note was written for more experienced people that would like to join the effort of producing 3D printed protective masks. It requires reasonable software management and very basic programming skills. We will guide you into the main parameters to adapt for your own tools and materials.

Most of the technical choices were made for anticipated and commented reasons : 

- 2 pins were primarily used to avoid central punch on the foil. A third one was placed because this two pins solution was too unpractical. Some other users wanted 4 pins, and we thus generalized the code to any number of pins.
- Height and thickness were chosen to minimise material use and speed up printings while keeping reasonable comfort.
- The size of the outer ring was set to maximum while keeping the capability to put two side by side on a Prusa MK3S printing bed. We are aware that a slightly bigger diameter could be more practical but that would reduce printing capabilities
- the default dimensions and pin positions were chosen so that any classical A4 transparent foil can be used instead of much more expensive ready-to-cut plates. We got plenty (~10 000) foils in a very short time.
- Dimensions and design were meant to be used with a double and very simple domestic rubber you can find in (nearly) every house.

## OpenSCAD installation

This project was written in [OpenSCAD](https://www.openscad.org/), a free and opensource 3D CAD software. This sofware use a program-style code.

OpenSCAD is available for [indows, Linux and Mac](https://www.openscad.org/downloads.html). Please refer to the documentation, except for Ubuntu 18.04 and above (see hereunder).

Special note for Ubuntu 18.04 and above : as OpenSCAD is not part of the basic repositories anymore, you should add the right source repository first and install it as follow :
```
sudo add-apt-repository ppa:openscad/releases
sudo apt-get update
sudo apt-get install openscad
```

## Code structure

The code was written in a very parametric way, which means that you can modify the piece main characteristics and dimensions very easily. All those parameters are declared at the beginning of the code. Nevertheless, be aware that most of those parameters have been fine-tuned to get the best results and production rates. Read and make sure you understand the comments before you change the parameters values.

## Main parameters

The best way to understand those parameters is to read the comment first, then play with one parameter at the time.

The main parameters are :
  - *layer_h* : printing layer height. As our goal is to produce at the highest rate we can, we use thick and drafty layers for printing. Our design is quite fine and some intentional gaps were left between stacked pieces. To get a proper printing you should ajust this parameter so every piece AND every gap is integer multiple of the printing layer height (auto-ajusted in the code further)
  - *stacked_n* : the number of stacked pieces. Use 1 for single piece (without stacking structure). Better use 3 for testing, and go higher for "mass-production".
  - *stacked_d* : the stack distance between two pieces. You should probably use 2 layers and thus leave the calculation to be done by the code as is.
  - *approx_h* : approximative height of each piece. The reason why it is approximative was alread explained above. The actual height will thus be the closest integer multiple of the *layer_h* paremeter.
  - *th* : thickness of the pieces. As this is not mandatory, you should use an integer multiple of your printing nozzle diameter. 1.2 was chosen by default as this ensure a proper robustness, and being an integer multiple of both 0.4 (3x) and 0.6 (2x) printing nozzles
  - *fix_dist* : the distance between the most distant pins => better express this value as your `foil dimension - 2*distance_from border`.
  - *fix_n* : number of fixing pins. Original design was made for 2 then 3 pins (asked by Saint Pierre hospital). But we were asked to adapt the code to 4 pins and then decided to make the code more generic, so you can now choose any number of pins. They will be spaced evenly apart.
  - *fix_d* and *fix_over* : this allows you to set the fixing pin internal diameter and the exceeding head. `fix_d + 2*fix_over ` should be slightly more than you **punch holes diameter*, so that the foil-pin connection will be tight-fit.
  - *fix_h* : the total height of the fixing pins
  - *sup_adri* : Adricen &lt;adrien.centonze@love-open-design.com&gt; contribution : adding vertical supports for pins into the scad file. Set to "true" to get vertical supports.
  - *hook_fence* : Pierre Hilson &lt;zorglups.mailing@gmail.com&gt; contribution : better dashed gap at archs meeting point & tighter hooks to hold rubber in place

Please refer to the code comments for the other parameters.
  
## Printing of stacked pieces

The code already ensure an easy-to-detach structure, automatically calculated based on your provided parameters.
Nevertheless, a pin-structure should be added to get proper printing. With Prusa Slic3r, the `Supports : for support enforcers only` should be used and proper cube blocks added as explained in this very nice [video tutorial](https://www.youtube.com/watch?v=yqMXfvWA-aY).

![](images/stacked.png)

## Know issues

### Manyfoil issue
Some generated stl can exhibit "manyfoil" problems. Slic3r allows to repair the `.stl` files and record it to an `.obj` file. With newer file version (>3.1) this shouldn't be an issue for the Slic3r, as long as you set the *layer_h* paremeter properly.

### PETG printing issue

Better know your printer before printing... this is a general advice.

With a Prusa MK3S printer, we have noticed that at room temeperature (~20-22°C) the first layer is much better with slightly increased temperatures (nozzle 244°C instead of 240°C for first layer by default, 88°C bed instead of 85°C by default) and lower printing speed (12mm/sec instead of 20mm/sec for the first layer by default). As this first layer is only a tiny fraction of the whole printing process, this slowing isn't a real issue for production rate and ensure a perfect adherence to the printing bed, which is very important for high stacking printings. You should adapt those parameters in your Slic3r first (advanced or expert mode).



## Overview

![](images/openscad.png)

## SCAD file

[anti_projection.scad](./anti_projection.scad)
